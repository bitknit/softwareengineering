import React, { Component } from 'react';
import '../App.css';
import './combined.css';
import {Button, ButtonGroup} from 'react-bootstrap';
import {Modal, ModalHeader, ModalBody} from 'reactstrap';
import {Link} from 'react-router-dom'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons'


class TableGen extends Component {
    constructor(props, context){
        console.log(props);
        super(props, context);
        if(this.props.type === "combined"){
            this.title = "All";
        }
        if(this.props.type === "books"){
            this.title = "Books";
        }
        if(this.props.type === "research-papers"){
            this.title = "Research Papers";
        }
        
        this.state = {
          modal: false
        };

        this.adderLink = "addnew/" + this.props.type;
        this.editorLink = "editlist/"+ this.props.type;
        this.toggle = this.toggle.bind(this);
        this.data = []; // This is the array containing all the information.
    }
    
    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    populateList(){
        // This function fetches the data from the database and populates the list.
        // TO be completed later on.
    }

    render(){
        return(
            <div className = "table container-fluid ">
                <Modal className = "small text-center" isOpen={this.state.modal} toggle={this.toggle} >
                    <ModalHeader toggle = {this.toggle}> Generate </ModalHeader>

                    <ModalBody>
                        Select format for <u>{this.title}</u> table to generate and download
                        or print.
                        <br /><br />
                        Table Format:
                        <form>
                            <select>
                                <option> Modern Language Association</option>
                                <option> Modern Language Association</option>
                                <option> Modern Language Association</option>
                                <option> Modern Language Association</option>
                            </select>
                        </form>

                        <br />
                        File Format:
                        <form>
                            <select>
                                <option>PDF</option>
                                <option>DOCX</option>
                            </select>
                        </form>
                        <br />
                        <ButtonGroup>
                            <Button bsStyle = "primary" bsSize = "small">Generate and Download</Button>
                            <Button bsStyle = "primary" bsSize = "small">Generate and Print</Button>
                        </ButtonGroup>
                        <br />&nbsp;
                    </ModalBody>
                </Modal>
                <div className = "container">
                    <h6>
                        {this.title}
                        <br />
                    </h6>
                    <br />
                    <table className = "small">
                        <tbody>
                            <tr>
                                <td> S.N.</td>
                                <td> Title </td>
                                <td> Authors </td>
                                <td> ISBN </td>
                                <td> ISSN </td>
                                <td> DOI </td>
                                <td> File </td>
                                <td> Edit </td>
                            </tr>
                            <tr>
                                <td>1. </td>
                                <td> An analysis of current Trends in IOT.</td>
                                <td> Sandesh Bhusal, Prasanga Dhungel </td>
                                <td> 097-89-0987225 </td>
                                <td> 009-11-1123-455 </td>
                                <td> doi.i/1123-123b-1 </td>
                                <td> <a href = "download_page"> Download </a></td>
                                <td> <FontAwesomeIcon className = "faIcon" icon={faPen} /></td>
                            </tr>
                            <tr>
                                <td> 2. </td>
                                <td> This is another publication by the user</td>
                                <td> Mr. User and his best Friend</td>
                                <td> 097-89-0987225 </td>
                                <td> 009-11-1123-455 </td>
                                <td> doi.i/1123-123b-1 </td>
                                <td> <a href = "download_page"> Download </a></td>
                                <td> <FontAwesomeIcon className = "faIcon"  icon={faPen} /></td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <ButtonGroup>
                        <Button bsSize = 'small' bsStyle = 'danger' onClick = {this.toggle}>Generate</Button>
                        <Button bsSize = 'small' bsStyle = 'warning'><Link to={this.editorLink} state={`type: this.props.type`} className="white">Edit List</Link></Button>
                        {
                            this.props.type != "combined"?
                            <Button bsSize = 'small' bsStyle = 'success'><Link to={this.adderLink} state={`type: this.props.type`} className="white">Add new Entry</Link></Button>
                            : ""
                        }
                    </ButtonGroup>
                </div>
            </div>
        );
    }
}

export default TableGen;