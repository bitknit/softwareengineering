import React, {Component} from 'react';
import Navibar from '../navbar';
import {FormControl} from 'react-bootstrap';
import './newentry.css';
import { BookFields, ResearchPaperFields } from './fields';
import {Button} from 'react-bootstrap';

class Entry extends Component{
    constructor(props){
        super(props);
        console.log("Props: ", props.match.params);
        if(props.match.params.type == "books"){
            this.type = "Book";
            this.fields = BookFields;
        }
        else if(props.match.params.type == "research-papers"){
            this.type = "Research paper";            
            this.fields = ResearchPaperFields;
        }
        else{
            this.type = "Research Paper";
            this.fields = ResearchPaperFields;
        }
    }
    render(){
        return(
            <div>
                <Navibar />
                <br />
                <div className = "container-fluid">
                    <div className = 'container'>
                        Add a new <b>{this.type}</b> to the database.
                        <br /><br />
                        <table id = "entryTable">
                        <tbody>
                        {
                            Object.keys(this.fields).map((key) => {
                                return (<tr key = {key}><td>{key}:</td><td><FormControl
                                        className = "input" 
                                        name = {key} 
                                        type =  {this.fields[key].type} 
                                        placeholder = {this.fields[key].placeholder}/></td></tr>)
                            })
                        }
                        </tbody>
                        </table>
                        <br />
                        <Button bsStyle = "danger"> Cancel </Button> &nbsp;
                        <Button bsStyle = "success">Add </Button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Entry;