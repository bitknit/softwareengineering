import React, {Component} from 'react';
import './footer.css';

class Footer extends Component{
    render(){
        return(
            <div className = "footer container-fluid">
                <div className = "container ">
                    Publication List Manager
                </div>
            </div>
        );
    }
}

export default Footer;