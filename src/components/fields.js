/*
    This file contains the fields in a type of data that is to be entered.
    Along with the associated title and the type of the data it collects.
*/

var BookFields = {
    Title: { placeholder: "Name of publication", type: "string"},
    Type: { placeholder: "What kind of publication? ", type: "string"},
    ISBN: { placeholder: "ISBN Number", type: "string"},
    ISSN: { placeholder: "ISSN Number ", type: "string"},
    Authors: { placeholder: "Author List. ", type: "string"}
}


var ResearchPaperFields = {
    Name: { placeholder: "Name of publication", type: "string"},
    Authors: { placeholder : "Enter the names of authors seperated by commas: ", type: "string"},
    Publisher: {placeholder : "Enter the name of the publisher, e.g. Wiley", type: "string"},
    Volume: {placeholder: "Enter the volume where the article was published", type: "string"},
    ISSN: { placeholder: "ISSN Number ", type: "string"}
}

export { BookFields, ResearchPaperFields };